# Gigodesign Coding Task 1

## 1.1. HTML razrez
Pripravi HTML razrez dizajna, ki smo ga s tabo delili na [InVision]-u. Na e-mail bi moral prejeti povabilo za sodelovanje; če ga nisi, nam sporoči. Če svojega InVision računa še nimaš, ga lahko brezplačno ustvariš.

## 1.2. PHP implementacija delujočega obrazca
HTML razrez dopolni tako, da bo obrazec za posredovanje sporočila deloval in poslal vsebino na tvoj e-mail naslov. Pri tem upoštevaj, da bomo v naslednji fazi razvoja sporočilo zapisali tudi v bazo.

[InVision]: <https://www.invisionapp.com/>